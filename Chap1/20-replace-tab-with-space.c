#include <stdio.h>
#include <stdlib.h>

/**
 * replace a tab with N whitespace
 */
int detab(int len)
{
    char ws[9];
    if( len>8 || len<0 )
        return -1;
    ws[len--]='\0';
    for( ;len>=0; --len )
        ws[len] = ' ';
    printf("ws is now (%s)\n", ws);
    int c;
    while( (c=getchar())!=EOF )
        if( '\t'==c )
            printf("%s", ws);
        else
            printf("%c", c);
    return 0;
}

int main(int argc, char* argv[])
{
    int len = 4;
    if( argc>1 )
    {
        int i;
        i = strtol(argv[1], NULL, 10);
        if( i<1 || i>8 )
        {
            printf("invalid len option:%s\n", argv[1]);
            return 1;
        }
        len = i;
    }
    detab(len);
    return 0;
}

