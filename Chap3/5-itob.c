#include <stdio.h>
#include <stdlib.h>

void reverse(char cs[], int len)
{
    int i=0, c;
    --len;
    while( i<len )
    {
        c = cs[i];
        cs[i++]=cs[len];
        cs[len--] = c;
    }
}

/**
 * convert int to string on the base of b
 */
int itob(int n, char s[], int b)
{
    if( b<2 || b>62 )
        return -1; // invalid base
    if( 0==n )
    {
        s[0] = '0';
        s[1] = '\0';
        return 0;
    }
    char sign='+';
    if( n<0 )
    {
        sign = '-';
        n = -n;
    }
    int r, p=0, lc=0;
    while( n>0 )
    {
        r = n%b;
        if( r<10 )
            s[p++] = '0'+r;
        else if( r<36 )
            s[p++] = 'a' + r - 10;
        else
            s[p++] = 'A' + r - 36;
        n /= b;
    }
    if( '-'==sign )
        s[p++]=sign;
    s[p] = '\0';
    reverse(s, p);
    return 0;
}

void test()
{
    char s[128];
    int nums[]={0,1,2,3,7,8,9,10,11,15,16,35,36,37,60,61,62,63,64};
    int base[] = {2,8,10,16,36,62};
    int n,b;
    for( b=0; b<6; ++b )
    {
        printf("=== Base %d ===\n", base[b]);
        for( n=0;n<19;++n )
        {
            itob(nums[n], s, base[b]);
            printf("%d => %s, ", nums[n], s);
            itob(-1*nums[n], s, base[b]);
            printf("%d => %s\n", -1*nums[n], s);
        }
    }
}

int main(int argc, char* argv[])
{
    test();
    return 0;
}
