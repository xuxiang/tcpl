#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

//max file name length
#define MAX_LENGTH 1024

int main(int argc, char* argv[])
{
    int i=1;
    char buf[MAX_LENGTH];
    for( ;i<argc; ++i )
    {
        if( strlen(argv[i])>MAX_LENGTH-1 )
        {
            fprintf(stderr, "Max file name length exceeded, skipping %s \n", argv[i]);
            continue;
        }
        if( NULL==realpath(argv[i], buf) )
        {
            fprintf(stderr, "error processing %s: %s\n", argv[i], strerror(errno) );
            continue;
        }
        printf("%s => %s\n", argv[1], buf );
    }
    return 0;
}

