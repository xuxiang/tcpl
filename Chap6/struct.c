#include <stdio.h>
#include <stdlib.h>

typedef struct _person Person;

struct _person {
    char* name;
    unsigned int age;
};

Person* clone(Person *p)
{
    Person *c;
    c = (Person*)malloc(sizeof(Person) );
    if( NULL!=c )
    {
        c->name = p->name;
        c->age = p->age;
    }
    return c;
}

typedef void (*hf)(Person*);

void hi1(Person *p)
{
    printf("Hi, I'm %s, %u years old.\n", p->name, p->age);
}

void hi2(Person *p)
{
    printf("Hello, I'm %s, %u years old.\n", p->name, p->age);
}

int main(int argc, char* argv[])
{
    Person me;
    me.name = "xu xiang";
    me.age = 26;
    hf f= &hi1;
    (*f)(&me);
    Person* m2 = clone(&me);
    if( NULL!=m2 )
    {
        f=&hi2;
        f(m2);
    }
    return 0;
}
