#include <stdio.h>
#include <stdlib.h>

typedef union {
    int age;
    char* name;
} Person;

int main(int argc, char* argv[])
{
    Person p1, p2;
    p1.age = 100;
    p2.name = "xuxiang";
    printf("%s == %d\n", p2.name, p1.age );
    return 0;
}
